import json
import re
import urllib2
from bs4 import BeautifulSoup

##################################################################
# Populates a single mention in the chatString starting at index i
##################################################################
def populateMentions( i ):
    j = i + 1 # Increment index since '@' has already been parsed.
    isMention = False
    
    while j < len(chatString):
        curr = chatString[j]
        
        # Ending condition for a valid mention
        if (j == (len(chatString) - 1) or not curr.isalnum()):
            if not curr.isalnum():
                # Retract index by 1, since it is not part of this mention
                # and could be part of a subsequent string
                j = j - 1

            mention = chatString[i + 1 : j + 1]

            # Add mention to the result only if its len > 0
            if(len(mention) > 0):
                mentions.append(mention)
                isMention = True
            
            break

        else:
            j = j + 1

    if(isMention):
        # Update i to the end of mention, since mention found
        i = j
    
    return i

###################################################################
# Populates a single emoticon in the chatString starting at index i
###################################################################
def populateEmoticons( i ):
    j = i + 1 # Increment index since '(' has already been parsed
    count = 0 # Keeps track of the number of characters in the emoticon
    isEmoticon = False
    emoticonCharLimit = 15 # This limit comes from the problem statement
    
    while j < len(chatString) and count < emoticonCharLimit + 1:
        count = count + 1
        curr = chatString[j]
        
        # Ending condition for a valid emoticon
        if curr == ')':
            emoticon = chatString[i + 1 : j]
            
            # Add emoticon to the result only if its len > 0
            if(len(emoticon) > 0):
                emoticons.append(emoticon)
                isEmoticon = True
            
            break
    
        # Ending condition for an invalid emoticon

        # According to the problem statement emoticons can contain
        # only ASCII characters. However, a test with the emoticon
        # "Shortcut text" field on https://www.hipchat.com/admin/emoticons
        # revealed that only alpha numeric characters are allowed and
        # that's the behavior this exercise implements. If we
        # were to check for all ASCII characters, that could be done as
        # follows -
        # if ord(curr) > 127: # 128 to 255 are non-ASCII codes
        elif not curr.isalnum():
            break
        
        else:
            j = j + 1

    if(isEmoticon):
        # Update i to the end of emoticon, since emoticon found
        i = j
    
    return i

###############################################################
# Populates a single link in the chatString starting at index i
###############################################################
def populateLinks( i ):
    # Use regex to find the first url that starts with http:// or https://
    # Other url schemas such as the ones that start with ftp, ws, wss,
    # gopher, or file are not considered a valid url for this exercise
    # as those urls won't have a title.
    m = re.match("(?P<url>https?://[^\s]+)", chatString[i:])
    
    # If regex found a match continue to process the url
    if m:
        isValidLink = True
        # This is the same group name as used in the regex match
        url = m.group("url")
        
        try:
            # It is necessary to set the timeout otherwise urlopen hangs
            # on bad urls or when the server may be down.
            # The timeout is set to be long enough for slow urls to load and short
            # enough to make the application appear to be responsive
            soup = BeautifulSoup(urllib2.urlopen(url, timeout = 3))
        except urllib2.URLError:
            isValidLink = False
        
        # Add the url to the list only if the url is valid and didn't timeout
        if isValidLink:
            title = soup.title.string # Get the title from the url
            linkObj = {}
            linkObj['url'] = url
            linkObj['title'] = title
            links.append(linkObj)
        
        # Return index after a url that started with http:// or https://
        # since a url was found even if it was not valid or timedout
    	i = i + len(url)

    return i

#----------------------------------
# Main entry point into the program
#----------------------------------
# Prompt the user to enter the chat string
chatString = raw_input('Enter your chat string: ')
mentions = []
emoticons = []
links = []
i = 0   # index that parses every character in the chatString

# Parse the chatString
while i < len(chatString):
    c = chatString[i]
    
    if c == '@':
        i = populateMentions( i )
    elif c == '(':
        i = populateEmoticons( i )
    else:
        i = populateLinks( i )

    i = i + 1

# Populate and print the final result
resultObj = {}
if(len(mentions) > 0):
    resultObj['mentions'] = mentions

if(len(emoticons) > 0):
    resultObj['emoticons'] = emoticons

if(len(links) > 0):
    resultObj['links'] = links

print json.dumps(resultObj, indent=1)


