======================
Readme for hipchat.py
======================

Pre-requisites:
===============
1. Install Python (I used Python 2.7)
2. Install BeautifulSoup library. This library is used to process the urls.
   Instructions  -
    - Download BeautifulSoup from url-> http://www.crummy.com/software/BeautifulSoup/bs4/download/4.3/ 
      You may also download it from my project on bitbucket -> https://bitbucket.org/masoomar/hipchat/src
    - cd into beautifulsoup4-4.3.2 from command prompt
    - Install BeautifulSoup using command:
        > python setup.py install
    Note: If you face permission denied issue, run with sudo.


Execute the project:
====================
1. Download the source code into a folder on your computer from https://bitbucket.org/masoomar/hipchat/src
2. Open command prompt and cd into that folder.
3. Run the following command:
    > python hipchat.py
4. You will be prompted to enter a chat string.
5. Enter a chat string and hit <ENTER>.
6. Output will be displayed in the command prompt.


Assumptions:
============

Mentions:
—————————
- A valid mention can only contain alphabets and numbers, but no special characters, as per the behavior I observed on hipchat.com.

Emoticons:
—————————-
- Emoticon is considered valid only if it has alphabets and numbers. This is based on the behavior noticed on https://www.hipchat.com/admin/emoticons.
According to the problem statement emoticon string can contain any ASCII character. However ASCII also includes spaces and tabs and other special characters which don’t seem to be allowed in the emoticon name on https://www.hipchat.com/admin/emoticons. For completeness, ASCII check code is also added in the code comments.

Links:
——————
- String is considered a URL if it starts with http:// or https:// and ends with a space or end of string. The program does not consider strings beginning with other schemas such as ftp://, ws:// file:// etc. as valid URLs since they don’t have a title that can be populated.

- URL will not be added to the result, if it is a bad URL or if the server is down. If the server is down, we try for 3 seconds and then time-out. Furthermore, this bad URL will be removed from the chatString and will not be parsed for possible mentions or emoticons instead.

- A URL that starts without http:// or https:// such as - www.something.com is not considered a valid URL link and that’s why the code doesn’t add it to the result. The code can be changed though such that http:// is appended to a link to make it a valid url.

Testing Methodology:
====================
I came up with a list of test cases along with the expected behavior and executed them manually.
These test cases are listed in file testcases.txt.


============================
Bugs noticed on hipchat.com:
============================
While using some features on hitchat.com, I came across a couple of bugs -

- When creating “Shortcut text” on https://www.hipchat.com/admin/emoticons, if you enter an invalid character, the message displayed is “Shortcut must only contain the characters a-z” even though it accepts numbers apart from characters a-z. 

- If you enter “abchttp://google.com” or “abc://google.com” in the chat window on hipchat.com, it makes the entire string a url, which seems incorrect.
